export const getClassName = (...args) => {
	let className = "";

	args.forEach((item) => {
		if (typeof item !== "object") {
			className = `${className} ${item.toString()}`;
		} else if (Object.prototype.toString.call(item) === "[object Object]") {
			Object.entries(item).forEach((item) => {
				if (item[1]) className = `${className} ${item[0]}`;
			});
		}
	});

	return className;
};
