export const board = [
	[
		{
			id: 1,
			label: "A1",
			row: 1,
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 2,
			row: 1,
			label: "B1",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 3,
			row: 1,
			label: "C1",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 4,
			row: 1,
			label: "D1",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 5,
			row: 1,
			label: "F1",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
	],
	[
		{
			id: 11,
			row: 2,
			label: "A2",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 22,
			row: 2,
			label: "B2",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 33,
			row: 2,
			label: "C2",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 44,
			row: 2,
			label: "D2",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 55,
			row: 2,
			label: "F2",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
	],
	[
		{
			id: 111,
			row: 3,
			label: "A3",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 222,
			row: 3,
			label: "B3",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 333,
			row: 3,
			label: "C3",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 444,
			row: 3,
			label: "D3",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 555,
			row: 3,
			label: "F3",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
	],
	[
		{
			id: 1111,
			row: 4,
			label: "A4",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 2222,
			row: 4,
			label: "B4",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 3333,
			row: 4,
			label: "C4",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 4444,
			row: 4,
			label: "D4",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 5555,
			row: 4,
			label: "F4",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
	],
	[
		{
			id: 11111,
			row: 5,
			label: "A5",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 22222,
			row: 5,
			label: "B5",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 33333,
			row: 5,
			label: "C5",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 44444,
			row: 5,
			label: "D5",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
		{
			id: 55555,
			row: 5,
			label: "F5",
			hasShip: false,
			isKilled: false,
			isMissed: false,
		},
	],
];
