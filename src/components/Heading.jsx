import React from "react";

import { constants } from "../constants";

const getPlayerLabel = (playerId) =>
	playerId === constants.FIRST_PLAYER ? "First Player" : "Second Player";

export const Heading = ({
	start,
	reset,
	playerId,
	canConfirmPick,
	confirmPick,
	canShowPlayer,
	isStageFinish,
}) => {
	return (
		<div className="mb_20">
			Battleship Game
			<button onClick={start}>Start</button>
			<button onClick={reset}>Reset</button>
			{canConfirmPick && <button onClick={confirmPick}>Confirm Pick</button>}
			{playerId > 0 && canShowPlayer && <h1>{getPlayerLabel(playerId)}</h1>}
			{playerId > 0 && isStageFinish && <h1> WINNER IS {getPlayerLabel(playerId)} !!</h1>}
		</div>
	);
};
