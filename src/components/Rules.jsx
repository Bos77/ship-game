import React from "react";

export const Rules = () => {
	return (
		<div className="d-flex">
			<div className="d-flex mr_20">
				<div className="rule__color blue"></div>
				<h3 className="rule__title">Ship position</h3>
			</div>

			<div className="d-flex mr_20">
				<div className="rule__color red"></div>
				<h3 className="rule__title">Ship missed</h3>
			</div>

			<div className="d-flex">
				<div className="rule__color black"></div>
				<h3 className="rule__title">Ship destroyed</h3>
			</div>
		</div>
	);
};
