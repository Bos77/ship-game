import { getClassName } from "../getClassName";

export const Square = ({
	text,
	row,
	ceilId,
	hasShip,
	isMissed,
	isKilled,
	isStageGame,
	toggleShip,
}) => {
	return (
		<div
			className={getClassName("square", {
				"square_has-ship": hasShip && !isStageGame,
				square_missed: isMissed,
				square_killed: isKilled,
				"pointer-events_none": isKilled || isMissed,
			})}
			onClick={() => toggleShip(row, ceilId)}
		>
			{text}
		</div>
	);
};
