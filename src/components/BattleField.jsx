import React from "react";

import { Square } from "./Square";
import { getClassName } from "../getClassName";

export const BattleField = ({ board, canPlay, toggleShip, isStageGame }) => {
	return (
		<>
			{board.map((item, index) => (
				<div
					className={getClassName("row", {
						"cursor_not-allowed": !canPlay,
					})}
					key={index}
				>
					{item.map((innerItem) => (
						<Square
							isStageGame={isStageGame}
							text={innerItem.label}
							key={innerItem.id}
							hasShip={innerItem.hasShip}
							ceilId={innerItem.id}
							row={innerItem.row}
							toggleShip={toggleShip}
							isKilled={innerItem.isKilled}
							isMissed={innerItem.isMissed}
						/>
					))}
				</div>
			))}
		</>
	);
};
