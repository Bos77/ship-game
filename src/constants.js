export const constants = {
	FIRST_PLAYER: 1,
	SECOND_PLAYER: 2,

	FIRST_STAGE: "Arrangement of ships",
	SECOND_STAGE: "Game",
	THIRD_STAGE: "Winner",
};
