import React from "react";

import { board } from "./board";
import { constants } from "./constants";

import { BattleField } from "./components/BattleField";
import { Heading } from "./components/Heading";
import { Rules } from "./components/Rules";

class App extends React.Component {
	maximumShip = 8;

	state = {
		stage: "",
		turn: constants.FIRST_PLAYER,
		firstPlayerLimit: 0,
		secondPlayerLimit: 0,
		firstPlayerFields: board,
		secondPlayerFields: board,
	};

	start = () => {
		this.setState({ stage: constants.FIRST_STAGE, turn: 1 });
	};

	reset = () => {
		this.setState({
			stage: "",
			turn: constants.FIRST_PLAYER,
			firstPlayerLimit: 0,
			secondPlayerLimit: 0,
			firstPlayerFields: board,
			secondPlayerFields: board,
		});
	};

	_createShip = (board, row, ceilId) => {
		const newBoard = [...board];
		let limit = 0;
		newBoard[row] = newBoard[row].map((ceil) => {
			if (ceil.id === ceilId) {
				limit = !ceil.hasShip ? limit + 1 : limit - 1;
				return { ...ceil, hasShip: !ceil.hasShip };
			}
			return ceil;
		});
		return { newBoard, limit };
	};

	createShip = (row, ceilId) => {
		const { turn, firstPlayerFields, secondPlayerFields, firstPlayerLimit, secondPlayerLimit } =
			this.state;

		if (turn === constants.FIRST_PLAYER && this._isLimit(firstPlayerLimit)) {
			const { newBoard, limit } = this._createShip(firstPlayerFields, row - 1, ceilId);

			this.setState({
				firstPlayerFields: newBoard,
				firstPlayerLimit: firstPlayerLimit + limit,
			});
		} else if (turn === constants.SECOND_PLAYER && this._isLimit(secondPlayerLimit)) {
			const { newBoard, limit } = this._createShip(secondPlayerFields, row - 1, ceilId);

			this.setState({
				secondPlayerFields: newBoard,
				secondPlayerLimit: secondPlayerLimit + limit,
			});
		}
	};

	_attackShip = (board, row, ceilId) => {
		let isStriked = false;
		const newBoard = [...board];
		newBoard[row] = newBoard[row].map((ceil) => {
			if (ceil.id === ceilId) {
				isStriked = ceil.hasShip;
				return { ...ceil, isKilled: isStriked, isMissed: !isStriked };
			}
			return ceil;
		});
		return { newBoard, isStriked };
	};

	attackShip = (row, ceilId) => {
		const { turn, firstPlayerFields, secondPlayerFields, firstPlayerLimit, secondPlayerLimit } =
			this.state;

		if (turn === constants.FIRST_PLAYER) {
			const { newBoard, isStriked } = this._attackShip(secondPlayerFields, row - 1, ceilId);
			const limit = isStriked ? secondPlayerLimit - 1 : secondPlayerLimit;

			this.setState({
				secondPlayerFields: newBoard,
				secondPlayerLimit: limit,
				turn: isStriked ? constants.FIRST_PLAYER : constants.SECOND_PLAYER,
			});
			this.isEndGame(limit);
		} else if (turn === constants.SECOND_PLAYER) {
			const { newBoard, isStriked } = this._attackShip(firstPlayerFields, row - 1, ceilId);
			const limit = isStriked ? firstPlayerLimit - 1 : firstPlayerLimit;

			this.setState({
				firstPlayerFields: newBoard,
				firstPlayerLimit: limit,
				turn: isStriked ? constants.SECOND_PLAYER : constants.FIRST_PLAYER,
			});
			this.isEndGame(limit);
		}
	};

	toggleShip = (row, ceilId) => {
		const { stage } = this.state;
		if (stage === constants.FIRST_STAGE) this.createShip(row, ceilId);
		else if (stage === constants.SECOND_STAGE) this.attackShip(row, ceilId);
	};

	confirmPick = () => {
		const { turn, firstPlayerLimit, secondPlayerLimit } = this.state;

		if (turn === constants.FIRST_PLAYER && !this._isLimit(firstPlayerLimit)) {
			this.setState({ turn: constants.SECOND_PLAYER });
		} else if (turn === constants.SECOND_PLAYER && !this._isLimit(secondPlayerLimit)) {
			this.setState({
				turn: constants.FIRST_PLAYER,
				stage: constants.SECOND_STAGE,
			});
		}
	};

	isEndGame = (limit) => {
		if (limit < 1) {
			this.setState({
				stage: constants.THIRD_STAGE,
			});
		}
	};

	_isLimit = (playerLimit) => playerLimit < this.maximumShip;

	_canBoardShow = (player) => {
		const { turn, stage } = this.state;
		return (
			turn === player || stage === constants.SECOND_STAGE || stage === constants.THIRD_STAGE
		);
	};

	_canPlay = (player) => {
		const { turn, stage } = this.state;

		if (stage === constants.SECOND_STAGE) return turn !== player;

		return turn === player && stage;
	};

	_isStageArrangement = () => this.state.stage === constants.FIRST_STAGE;
	_isStageGame = () => this.state.stage === constants.SECOND_STAGE;
	_isStageFinish = () => this.state.stage === constants.THIRD_STAGE;

	render() {
		const { stage, turn, firstPlayerFields, secondPlayerFields } = this.state;

		return (
			<div className="battlefield">
				<div>
					<Rules />

					<Heading
						playerId={turn}
						start={this.start}
						reset={this.reset}
						confirmPick={this.confirmPick}
						canConfirmPick={this._isStageArrangement()}
						canShowPlayer={this._isStageGame() || this._isStageArrangement()}
						isStageFinish={this._isStageFinish()}
					/>

					<div className="d-flex">
						{stage ? (
							<>
								{this._canBoardShow(constants.FIRST_PLAYER) && (
									<div className="mr_20">
										<BattleField
											board={firstPlayerFields}
											toggleShip={this.toggleShip}
											canPlay={this._canPlay(constants.FIRST_PLAYER)}
											isStageGame={this._isStageGame()}
										/>
									</div>
								)}

								{this._canBoardShow(constants.SECOND_PLAYER) && (
									<div>
										<BattleField
											board={secondPlayerFields}
											toggleShip={this.toggleShip}
											canPlay={this._canPlay(constants.SECOND_PLAYER)}
											isStageGame={this._isStageGame()}
										/>
									</div>
								)}
							</>
						) : (
							<>
								<div className="mr_20">
									<BattleField board={board} />
								</div>
								<div>
									<BattleField board={board} />
								</div>
							</>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default App;
